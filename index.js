var Tesseract = require('tesseract.js')
var filename =  'files/ara.jpg'

const { TesseractWorker } = Tesseract;
const worker = new TesseractWorker();

worker.recognize(filename, 'ara')
    .then(function (result) {
        console.log('result', result.text)
    });